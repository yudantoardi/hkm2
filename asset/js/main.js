$(document).ready(function(){

    $(".view-bt").click(function(vb){
        vb.preventDefault();

        $($(this).children()).toggleClass("fa-list fa-th-large");
        $(".prod-list").toggleClass("list-view");
    });

    $(".tab-bt li a").click(function(tl){
        tl.preventDefault();

        $(".tab-bt li").removeClass("active");
        $($(this).parent()).addClass("active");

        $(".tab-ct").removeClass("show");
        $($(this).attr("href")).addClass("show");
    });

    $(".burger").click(function(b){
        b.preventDefault();
        $(".mob-nav").toggleClass("show");
        $($(this).children("i")).toggleClass("fa-bars fa-times");
    });

    $(".filter-bt").click(function(){
        $(".master").addClass("product-filter-active");
        $(".sidebar").addClass("show");
        $(".sidebar .close-bt").addClass("show");
    });
    $(".sidebar .close-bt").click(function(){
        $(".master").removeClass("product-filter-active");
        $(".sidebar").removeClass("show");
        $(".sidebar .close-bt").removeClass("show");
    });

});